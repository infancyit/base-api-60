<?php
namespace App\BaseSettings;
class Settings
{
    public static $company_name = 'InfancyIT';
    public static $footer_text = 'InfancyIT';
    public static $roles = [
        'admin', 'user'
    ];
    public static $admin_role = 'admin';
    public static $client_role = 'user';
    public static $upload_path = 'uploads/';
}