<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Requests\Auth\UserLogin;
use App\Http\Requests\Auth\UserRegistration;
use App\Responses\ApiResponse;
use App\Services\Auth\ApiAuthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{
    /**
     * @var ApiResponse
     */
    private $apiResponse;
    /**
     * @var ApiAuthService
     */
    private $authService;

    /**
     * AuthController constructor.
     * @param ApiResponse $apiResponse
     * @param ApiAuthService $authService
     */
    public function __construct(ApiResponse $apiResponse, ApiAuthService $authService)
    {
        $this->apiResponse = $apiResponse;
        $this->authService = $authService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserLogin $request)
    {
        return $this->authService->login($request);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function logout()
    {
        return $this->authService->logout();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function refreshToken()
    {
        return $this->authService->getRefreshToken();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return $this->authService->me();
    }

    /**
     * @param UserRegistration $request
     * @return mixed
     * @throws \Exception
     */
    public function register(UserRegistration $request)
    {
        return $this->authService->register($request);
    }
}
