<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Controllers\Controller;
use App\Services\Auth\WebAuthService;

class AuthController extends Controller
{

    public function __construct(WebAuthService $webAuthService)
    {
        $this->webAuthService = $webAuthService;
    }


    public function showChangePasswordForm(){
        return view('admin.changePassword');
    }

    public function updatePassword(ChangePasswordRequest $request){
        $this->webAuthService->updatePassword($request);
        return redirect()->route('dashboard')->with('success', 'Password Changed Successfully');
    }
}
