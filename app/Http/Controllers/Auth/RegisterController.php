<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\UserRegistration;
use Exception;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\Auth\WebAuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'login';
    /**
     * @var WebAuthService
     */
    private $webAuthService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(WebAuthService $webAuthService)
    {
        $this->middleware('guest');
        $this->webAuthService = $webAuthService;
    }

    public function showRegistrationForm(){
        return view('auth.register');
    }

    public function register(UserRegistration $request)
    {
        try {
            $user = $this->webAuthService->register($request);
            if ($user) {
                return redirect()->route($this->redirectTo)->with('success',
                    'Welcome, Your account created successfully.');
            }
        } catch (Exception $exception) {
            return redirect()->back()->withInput()->with('error', 'something went wrong. Try again.');
        }
    }
}
