<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blog\BlogCreateRequest;
use App\Http\Requests\Blog\BlogUpdateRequest;
use App\Services\BlogService;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * @var BlogService
     */
    private $blogService;

    /**
     * BlogController constructor.
     * @param BlogService $blogService
     */
    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }

    public function index(){
        $blogs = $this->blogService->getFilterWithPaginatedData([]);
        return view('admin.blog.index', compact(['blogs']));
    }

    public function create(){
        $statuses = [ 'ACTIVE' , 'INACTIVE' ];
        return view('admin.blog.create', compact(['statuses']));
    }

    public function store(BlogCreateRequest $request){
        $data = $request->only(['title', 'description', 'status']);
        $blog = $this->blogService->create($data);
        return redirect()->route('blog.index')->with('success', 'Created Successfully');
    }

    public function show($id){
        $blog = $this->blogService->find($id);
        return view('admin.blog.show', compact(['blog']));
    }


    public function edit($id){
        $blog = $this->blogService->find($id);
        $statuses = [ 'ACTIVE' , 'INACTIVE' ];
        return view('admin.blog.edit', compact(['blog','statuses']));
    }

    public function update(BlogUpdateRequest $request, $id){
        $data = $request->only(['title', 'description', 'status']);
        $blog = $this->blogService->update($data, $id);
        return redirect()->route('blog.index')->with('success', 'Updated Successfully');
    }

    public function delete($id){
        $blog = $this->blogService->delete($id);
        return redirect()->route('blog.index')->with('success', 'Deleted Successfully');
    }
}
