<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Responses\ApiResponse;
use App\Transformers\Api\BlogTransformer;

class TestController extends Controller
{
    /**
     * @var ApiResponse
     */
    private $apiResponse;

    /**
     * TestController constructor.
     */
    public function __construct(ApiResponse $apiResponse)
    {
        $this->apiResponse = $apiResponse;
        $this->middleware(['auth','verified']);
    }

    public function index(){
        $blogs = Blog::all();
        return $this->apiResponse->collection($blogs, new BlogTransformer());
    }
}
