<?php

namespace App\Responses;

use App\Serializers\ApiSerializers;
use EllipseSynergie\ApiResponse\AbstractResponse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Request;
use League\Fractal\Manager;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class ApiResponse
 *
 * @package App\Responses
 */
class ApiResponse extends AbstractResponse
{

    const HTTP_CODE_200_OK = 200;
    const HTTP_CODE_201_CREATED = 201;
    const HTTP_CODE_400_BAD_REQUEST = 400;
    const HTTP_CODE_401_UNAUTHORIZED = 401;
    const APP_CODE_200_OK = 200;
    const APP_CODE_400_INVALID_REQUEST = 400;

    /**
     * ApiResponse constructor.
     */
    public function __construct()
    {
        parent::__construct(new Manager());
        $this->manager->setSerializer(new ApiSerializers());
        $this->parseIncludes(Request::input('include'));
    }

    /**
     * Respond success with simple response
     *
     * @param array|string $data
     * @param int $httpCode
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function success($data, $httpCode = self::HTTP_CODE_200_OK, array $headers = [])
    {
        if (!is_array($data)) {
            $data = ['message' => $data];
        } else {
            $data = ['message' => implode('\n', $data)];
        }
        return $this->setStatusCode($httpCode)
            ->withArray($data, $headers);
    }

    /**
     * Respond error with simple response
     *
     * @param string $errors
     * @param int $errorCode
     * @param string $details
     * @param array $headers
     * @return ResponseFactory
     */
    public function error($errors = 'Something went wrong.',
                          $errorCode = self::HTTP_CODE_400_BAD_REQUEST,
                          $details = '',
                          $headers = [])
    {
        if (!is_array($errors)) {
            $errors = ['error' => $errors, 'details' => $details];

        } else {
            $errors = ['message' => implode('\n', $errors), 'details' => $details];
        }
        return $this->setStatusCode($errorCode)
            ->withArray($errors, $headers);
    }

    /**
     * @param $includes
     * @internal param $connection
     * @return mixed
     */
    public function parseIncludes($includes)
    {
        if (is_null($includes)) {
            return false;
        }
        $this->manager->parseIncludes($includes);
    }

    /**
     * @param mixed $data
     * @param \League\Fractal\TransformerAbstract|callable $transformer
     * @param string $resourceKey
     * @param array $meta
     * @param array $headers
     * @return array
     */
    public function item($data, $transformer = null, $resourceKey = null, $meta = [], array $headers = [])
    {
        return $this->withItem($data, $transformer, $resourceKey, $meta, $headers);
    }

    /**
     * @param $data
     * @param \League\Fractal\TransformerAbstract|callable $transformer
     * @param string $resourceKey
     * @param Cursor|null $cursor
     * @param array $meta
     * @param array $headers
     * @return array
     */
    public function collection($data, $transformer = null, $resourceKey = null, Cursor $cursor = null, $meta = [], array $headers = [])
    {
        return $this->withCollection($data, $this->getTransformer($transformer), $resourceKey, $cursor, $meta, $headers);
    }

    /**
     * @param LengthAwarePaginator $paginator
     * @param \League\Fractal\TransformerAbstract|callable $transformer
     * @param string $resourceKey
     * @param array $meta
     * @return mixed
     */
    public function paginatedCollection(LengthAwarePaginator $paginator, $transformer = null, $resourceKey = null, $meta = [])
    {
        return $this->withPaginator($paginator, $this->getTransformer($transformer), $resourceKey, $meta);
    }


    /**
     * @param array $array
     * @param array $headers
     * @param int $json_options @link http://php.net/manual/en/function.json-encode.php
     * @return ResponseFactory
     */
    public function withArray(array $array, array $headers = [], $json_options = 0)
    {
        return response()->json($array, $this->statusCode, $headers, $json_options);
    }

    /**
     * Respond with a paginator, and a transformer.
     *
     * @param LengthAwarePaginator $paginator
     * @param callable|\League\Fractal\TransformerAbstract $transformer
     * @param string $resourceKey
     * @param array $meta
     * @return ResponseFactory
     */
    public function withPaginator(LengthAwarePaginator $paginator, $transformer, $resourceKey = null, $meta = [])
    {
        $queryParams = array_diff_key($_GET, array_flip(['page']));
        $paginator->appends($queryParams);

        $resource = new Collection($paginator->items(), $transformer, $resourceKey);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        foreach ($meta as $metaKey => $metaValue) {
            $resource->setMetaValue($metaKey, $metaValue);
        }

        $rootScope = $this->manager->createData($resource);

        return $this->withArray($rootScope->toArray());
    }

    /**
     * Generates a Response with a 400 HTTP header and a given message from validator
     *
     * @param Validator $validator
     * @return ResponseFactory
     */
    public function errorWrongArgsValidator(Validator $validator)
    {
        return $this->errorWrongArgs($validator->getMessageBag()->toArray());
    }

    /**
     * @param TransformerAbstract $transformer
     * @return TransformerAbstract|callback
     */
    protected function getTransformer($transformer = null)
    {
        return $transformer ?: function ($data) {
            if ($data instanceof Arrayable) {
                return $data->toArray();
            }
            return (array)$data;
        };
    }

}
