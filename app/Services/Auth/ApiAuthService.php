<?php
/**
 * Created by PhpStorm.
 * User: vivacom
 * Date: 6/1/17
 * Time: 5:02 PM
 */

namespace App\Services\Auth;


use App\BaseSettings\Settings;
use App\Responses\ApiResponse;
use App\Services\UserService;
use App\Transformers\Api\V1\Auth\LoginTransformer;
use App\Transformers\Api\V1\UserTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;


class ApiAuthService
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var ApiResponse
     */
    private $apiResponse;

    /**
     * ApiAuthService constructor.
     * @param UserService $userService
     * @param ApiResponse $apiResponse
     */
    public function __construct(UserService $userService, ApiResponse $apiResponse)
    {
        $this->userService = $userService;
        $this->apiResponse = $apiResponse;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->apiResponse->error("Invalid Credentials!");
            }
            return $this->apiResponse->item([
                'token' => $token
            ], new LoginTransformer());
        } catch (Exception $e) {
            return $this->apiResponse->error("Could not create token.");
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
        } catch (Exception $e) {
            throw new Exception("Invalid Token! Please login again.", 401);
        }
        return $this->apiResponse->success("Logout Successful");
    }

    /**
     * @return mixed
     */
    public function me()
    {
        $user = Auth::user();
        return $this->apiResponse->item($user, new UserTransformer());
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getRefreshToken()
    {
        try {
            $token = JWTAuth::refresh(JWTAuth::getToken());
            return $this->apiResponse->item([
                'token' => $token
            ], new LoginTransformer());
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), 401);
//            throw new Exception("Invalid Token! Please login again.", 401);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function register(Request $request)
    {
        try {
            $password = bcrypt($request->get('password'));
            $request->merge(['password' => $password]);
            $data = $request->only(['name', 'email', 'password']);
            $user = $this->userService->create($data);
            $user->assignRole(Settings::$client_role);
            $user->sendEmailVerificationNotification();
            return $this->apiResponse->item($user, new UserTransformer());
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
