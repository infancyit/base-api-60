<?php

namespace App\Transformers\Api;

use App\Transformers\ApiTransformerAbstract;

class BlogTransformer extends ApiTransformerAbstract
{

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */
    public function getTransformableFields($entity)
    {
        return [
            'id' => (int) $entity->id,
            'title' => $entity->title,
            'description' => $entity->description,
            'status' => $entity->status
        ];
    }
}