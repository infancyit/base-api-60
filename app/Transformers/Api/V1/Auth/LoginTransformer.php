<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 4/23/18
 * Time: 11:11 AM
 */

namespace App\Transformers\Api\V1\Auth;

use App\BaseSettings\Settings;
use App\Models\User;
use App\Transformers\Api\V1\UserTransformer;
use App\Transformers\ApiTransformerAbstract;
use Carbon\Carbon;

class LoginTransformer extends ApiTransformerAbstract
{
    protected $defaultIncludes = [
        'user'
    ];

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */
    public function getTransformableFields($entity)
    {
        return [
            'access_token' => $entity['token'],
            'token_type' => 'bearer',
            'expires_in' => Carbon::now()->addMinutes(config('jwt.ttl'))->timestamp,
        ];
    }

    public function includeUser($entity)
    {
        $user = auth()->user();
        if($user) {
            return $this->item($user, new UserTransformer());
        }
        return null;
    }
}
