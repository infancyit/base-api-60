<?php

namespace App\Transformers\Api\V1;

use App\Transformers\ApiTransformerAbstract;

class RoleTransformer extends ApiTransformerAbstract
{

    protected $availableIncludes = [
        // 'permission'
    ];

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */
    public function getTransformableFields($entity)
    {
        return [
            'name' => $entity->name,
        ];
    }


}
