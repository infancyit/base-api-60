<?php

namespace App\Transformers\Api\V1;

use App\Transformers\ApiTransformerAbstract;
use Illuminate\Support\Collection;

class UserTransformer extends ApiTransformerAbstract
{

    protected $availableIncludes = [
        'roles'
    ];

    /**
     * Get the fields to be transformed.
     *
     * @param $entity
     *
     * @return mixed
     */
    public function getTransformableFields($entity)
    {
        return [
            'id' => (int)$entity->id,
            'name' => $entity->name,
            'email' => $entity->email,
        ];
    }

    public function includeRoles($entity)
    {
        $roles = $entity->roles;
        if($roles)
        {
            return $this->collection($roles, new RoleTransformer());
        }
        return $this->collection([], new RoleTransformer(), 'roles');
    }

}
