<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Auth::routes(['verify' => true]);
//JWTAuth::routes(['verify' => true]);

Route::group(['prefix' => 'v1'], function () {
    Route::get('ping', function () {
        return response()->json('pong');
    });
    Route::group(['namespace' => 'Api\V1\Auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
    });

    Route::group(['namespace' => 'Api\V1\Auth', 'middleware' => ['jwt-auth']], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('me', 'AuthController@me');
        Route::post('refresh-token', 'AuthController@refreshToken');
    });
});
